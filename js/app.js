/// <reference path="../typings/react/react-global.d.ts" />
/// <reference path="./interfaces/interfaces.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var app;
(function (app) {
    var components;
    (function (components) {
        var Demo = (function (_super) {
            __extends(Demo, _super);
            function Demo(props) {
                _super.call(this, props);
            }
            Demo.prototype.render = function () {
                return React.createElement("h1", null, "Hello Mr. ", this.props.firstName, " ", this.props.lastName, ". ", this.props.age, " is a great age!");
            };
            return Demo;
        }(React.Component));
        components.Demo = Demo;
    })(components = app.components || (app.components = {}));
})(app || (app = {}));
/// <reference path="../typings/react/react-global.d.ts" />
/// <reference path="./interfaces/interfaces.d.ts" />
/// <reference path="./Table.tsx" />
var Table = app.components.Demo;
var container = document.getElementById('mountNode');
ReactDOM.render(React.createElement(Table, {age: 21, lastName: "Legge", firstName: "Chris"}), container);
//# sourceMappingURL=app.js.map