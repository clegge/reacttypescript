/// <reference path="../typings/react/react-global.d.ts" />
/// <reference path="./interfaces/interfaces.d.ts" />

namespace app.components {
    export class Demo extends React.Component<IDemo, any> {

        constructor(props:IDemo) {
            super(props);
        }

        render() {
            return <h1>Hello Mr. {this.props.firstName} {this.props.lastName}. {this.props.age} is a great age!</h1>
        }
    }
}
